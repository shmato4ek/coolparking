﻿using System;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking
{
    class Program
    {
        static void Main(string[] args)
        {
            Settings settings = Settings.GetInstance();
            WidthdrowTimerService withdrowTimer = new WidthdrowTimerService();
            LogTimerService logTimer = new LogTimerService();
            LogService logService = new LogService();
            using (ParkingService service = new ParkingService(withdrowTimer, logTimer, logService))
            {
                service.AllTimersStart();
                bool flag = true;
                while (flag)
                {
                    Console.WriteLine("Welcome to our Cool Parking!!!\n\n\tWhat do you want?\n\n");
                    Console.WriteLine("\t1) Display the current balance of the Parking lot.\n" +
                        "\t2) Display the number of free / occupied parking spaces.\n" +
                        "\t3) Display all Parking Transactions for the current period.\n" +
                        "\t4) Print the transaction history (by reading data from the Transactions.log file).\n" +
                        "\t5) Display the list Tr. funds located in the Parking lot.\n" +
                        "\t6) Put the Vehicle in the Parking Lot.\n" +
                        "\t7) Pick up a vehicle from the Parking lot.\n" +
                        "\t8) Top up the balance of a specific Vehicle.\n" +
                        "\t9) Generate random ID. \n" +
                        "\t0) Exit\n\n");
                    int answer = int.Parse(Console.ReadLine());
                    switch (answer)
                    {
                        case 1:
                            Console.WriteLine("\nBalance: {0}", service.GetBalance());
                            break;
                        case 2:
                            var freePlaces = service.GetFreePlaces();
                            var capacity = service.GetCapacity();
                            Console.WriteLine("Now are free {0} places from {1}", freePlaces, capacity);
                            break;
                        case 3:
                            Console.WriteLine("\tLast transactions\n\n");
                            var transactions = service.GetLastParkingTransactions();
                            foreach (var transaction in transactions)
                            {
                                Console.WriteLine(transaction.Date + ": " + transaction.VehicleId + " (sum: " + transaction.Sum + ")");
                            }
                            break;
                        case 4:
                            Console.WriteLine("\n\nTransactions from logs");
                            string logs = service.Read();
                            Console.WriteLine(logs);
                            service.Dispose();
                            break;
                        case 5:
                            Console.WriteLine("\n\n\tVehicles in the Parking");
                            var vehicles = service.GetVehicles();
                            foreach (var v in vehicles)
                            {
                                Console.WriteLine(v.Id + " (" + v.VehicleType + ")");
                            }
                            break;
                        case 6:
                            Console.Write("\n\n\tPlease, enter vehicle id: ");
                            string id = Console.ReadLine();
                            Console.WriteLine("\n\n\tChoose type of vehicle:\n\n");
                            Console.WriteLine("0) Passenger Car\n" +
                                "1) Truck\n" +
                                "2) Bus\n" +
                                "3) Motorcycle\n\n");
                            int typeId = int.Parse(Console.ReadLine());
                            var type = service.GetVehicleType(typeId);
                            Console.Write("Write a sum you want to put on vehicle`s account: ");
                            decimal sum = decimal.Parse(Console.ReadLine());
                            try
                            {
                                var vehicle = service.CreateVehicle(id, type, sum);
                                service.AddVehicle(vehicle);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Wrong data was writed!!!");
                                break;
                            }
                            var lastVehicle = service.GetLastVehicle();
                            Console.WriteLine("Vehicle {0} with id {1} and balance {2} was succesfully created!", lastVehicle.VehicleType, lastVehicle.Id, lastVehicle.Balance);
                            break;
                        case 7:
                            Console.Write("\n\nWrite id of vehicle: ");
                            string VehicleId = Console.ReadLine();
                            service.RemoveVehicle(VehicleId);
                            Console.WriteLine("\n\nVehicle with id {0} was successfuly removed!\n\n", VehicleId);
                            break;
                        case 8:
                            Console.Write("\n\nWrite id of vehicle: ");
                            string Id = Console.ReadLine();
                            Console.Write("\n\nWrite your sum: ");
                            decimal payment = decimal.Parse(Console.ReadLine());
                            service.TopUpVehicle(Id, payment);
                            Console.WriteLine("Balance of {0} has been increased by {1}", Id, payment);
                            break;
                        case 9:
                            Console.WriteLine("\n{0}\n", service.GenerateRandomID());
                            break;
                        case 0:
                            flag = false;
                            break;
                        default:
                            Console.WriteLine("\n\nWrong command!\n\n");
                            break;
                    }
                }
            }
        }
    }
}
