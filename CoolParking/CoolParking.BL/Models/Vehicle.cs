﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Collections.Generic;
using CoolParking.BL.Models;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private static List<Vehicle> allVehicles = new List<Vehicle>();
        public string Id { get; private set; }
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; set; }
        public Vehicle(string _Id, VehicleType _vehicleType, decimal _Balance)
        {
            try
            {
                if (_Id.Length<=9)
                {
                    throw new Exception();
                }
                if (_Balance <= 0)
                {
                    throw new Exception();
                }
            }

            catch (Exception e)
            {
                throw new ArgumentException();
            }
            Id = _Id;
            VehicleType = _vehicleType;
            Balance = _Balance;
            //Vehicle vehicle = new Vehicle (_Id, _vehicleType, _Balance);
            //allVehicles.Add(vehicle);
        }
        public void AddMoney(decimal sum)
        {
            Balance += sum;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();
            string idPartOne = "";
            string idPartTwo = "";
            string idPartThree = "";
            for (int i = 0; i < 2; i++)
            {
                idPartOne += (char)random.Next('A', 'Z');
                idPartThree += (char)random.Next('A', 'Z');
            }
            for (int i = 0; i < 4; i++)
            {
                idPartTwo += random.Next(0, 9).ToString();
            }
            return idPartOne + "-" + idPartTwo + "-" + idPartThree;
        }
        public static List<Vehicle> GetVehicles()
        {
            return Vehicle.allVehicles;
        }
    }
}