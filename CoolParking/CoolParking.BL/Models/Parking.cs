﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        private Settings settings = Settings.GetInstance();
        private List<TransactionInfo> transactions;
        private decimal Balance;
        public static List<Vehicle> vehicles;
        private Parking() { }
        private static Parking _instance;
        public static Parking GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Parking();
                _instance.transactions = new List<TransactionInfo>();
                vehicles = new List<Vehicle>();
            }
            return _instance;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            var allVehicles = Vehicle.GetVehicles();
            foreach (var veh in allVehicles)
            {
                if (veh.Id == vehicle.Id)
                {
                    throw new ArgumentException();
                }
            }
            allVehicles.Add(vehicle);
        }
        public decimal GetBalance()
        {
            return Balance;
        }
        public void AddMoney(decimal money)
        {
            Balance += money;
        }
        public int GetCountOfCurentVehicles()
        {
            return vehicles.Count;
        }
        public TransactionInfo[] GetTransactions()
        {
            return transactions.ToArray();
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> allVehicles = new ReadOnlyCollection<Vehicle>(Vehicle.GetVehicles());
            return allVehicles;
        }
        public void DeleteVehicle(Vehicle vehicle)
        {
            Vehicle.GetVehicles().Remove(vehicle);
        }
        public void AddTransaction (TransactionInfo transaction)
        {
            transactions.Add(transaction);
        }
    }
}