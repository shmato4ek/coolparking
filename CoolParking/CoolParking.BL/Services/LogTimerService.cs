﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.BL.Services
{
    public class LogTimerService : ITimerService
    {
        private static System.Timers.Timer aTimer;
        private Settings settings = Settings.GetInstance();
        public double Interval { get => settings.LoggingPeriod; set => settings.SetLoggingPeriod(value); }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            aTimer.Dispose();
        }

        public void Start()
        {
            aTimer = new System.Timers.Timer(Interval);
            Elapsed = new ElapsedEventHandler(OnTimedEvent);
            aTimer.Elapsed += Elapsed;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        public void Stop()
        {
            aTimer.Stop();
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            var parking = Parking.GetInstance();
            var settings = Settings.GetInstance();
            LogService service = new LogService();
            var transactionsList = parking.GetTransactions();
            foreach (var transaction in transactionsList)
            {
                service.Write(String.Format("Date = {0}, ID = {1}, sum = {2}", transaction.Date, transaction.VehicleId, transaction.Sum));
            }
        }
}
}
