﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking parking = Parking.GetInstance();
        private Settings settings = Settings.GetInstance();
        private ITimerService withdrawTimer;
        private ITimerService logTimer;
        private ILogService logService;
        public ParkingService(ITimerService _withdrowTimer, ITimerService _logTimer, ILogService _logService)
        {
            withdrawTimer = _withdrowTimer;
            logTimer = _logTimer;
            logService = _logService;
        }
        public string Read()
        {
            return logService.Read();
        }
        public void AddVehicle(Vehicle vehicle)
        {
            try
            {
                parking.AddVehicle(vehicle);
            }
            catch (ArgumentException)
            {

            }
        }

        public void AllTimersStart()
        {
            withdrawTimer.Start();
            logTimer.Start();
        }

        public void Dispose()
        {
            withdrawTimer.Dispose();
            logTimer.Dispose();
            if (File.Exists(logService.LogPath))
            {
                File.Delete(logService.LogPath);
            }
        }

        public decimal GetBalance()
        {
            return parking.GetBalance();
        }

        public int GetCapacity()
        {
            return settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            int ammountOfVehicles = parking.GetCountOfCurentVehicles();
            int parkingCapacity = settings.ParkingCapacity;

            if (ammountOfVehicles == 0)
            {
                return parkingCapacity;
            }

            else
            {
                return parkingCapacity - ammountOfVehicles;
            }

        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return parking.GetTransactions();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return parking.GetVehicles();
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var allVehicles = parking.GetVehicles();
            bool isCorrect = false;
            foreach (var veh in allVehicles)
            {
                if (veh.Id == vehicleId)
                {
                    isCorrect = true;
                }
            }
            if (!isCorrect)
            {
                throw new ArgumentException();
            }

            var vehicle = TakeVehicle(vehicleId);
                if (!allVehicles.Any())
                {
                    throw new Exception();
                }

                if (!allVehicles.Contains(vehicle))
                {
                    throw new ArgumentException();
                }
            parking.DeleteVehicle(vehicle);

        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var allVehicles = Vehicle.GetVehicles();
            bool isCorrect = true;
            foreach (var veh in allVehicles)
            {
                if (veh.Id == vehicleId)
                {
                    isCorrect = false;
                }
                if (sum < 0)
                {
                    isCorrect = true;
                }
            }
            if (isCorrect)
            {
                throw new ArgumentException();
            }
            var vehicle = TakeVehicle(vehicleId);
            vehicle.AddMoney(sum);
        }

        public Vehicle TakeVehicle(string id)
        {
            var allVehicles = parking.GetVehicles();
            List<Vehicle> vehicles = new List<Vehicle>(allVehicles);
            var resultVehicle = from vehicle in vehicles
                                where vehicle.Id == id
                                select vehicle;
            return resultVehicle.FirstOrDefault();
        }
        public Vehicle CreateVehicle(string _id, VehicleType _type, decimal balance)
        {
            Vehicle vehicle = new Vehicle(_id, _type, balance);
            return vehicle;
        }
        public VehicleType GetVehicleType(int i)
        {
            return (VehicleType)i;
        }
        public Vehicle GetLastVehicle()
        {
            var lastVehicle = parking.GetVehicles().LastOrDefault();
            return lastVehicle;
        }
        public string GenerateRandomID()
        {
            return Vehicle.GenerateRandomRegistrationPlateNumber();
        }
    }
}