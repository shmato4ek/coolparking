﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.


using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class WidthdrowTimerService : ITimerService
    {
        private static System.Timers.Timer aTimer;
        private Settings settings = Settings.GetInstance();

        public double Interval { get => settings.PaymentDebitingPeriod; set => settings.SetPaymentDebitingPeriod(value); }
        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            aTimer.Dispose();
        }

        public void Start()
        {
            aTimer = new System.Timers.Timer(Interval);
            Elapsed = new ElapsedEventHandler(OnTimedEvent);
            aTimer.Elapsed += Elapsed;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        public void Stop()
        {
            aTimer.Stop();
        }
        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            var parking = Parking.GetInstance();
            var settings = Settings.GetInstance();
            var vehicles = parking.GetVehicles();
            LogService service = new LogService();
            foreach (var vehicle in vehicles)
            {
                var sum = from price in settings.Prices
                          where price.Key == vehicle.VehicleType
                          select price.Value;
                decimal payment = sum.FirstOrDefault();
                Pay(vehicle.Id, payment);
                TransactionInfo transaction = new TransactionInfo { Date = e.SignalTime, Sum = payment, VehicleId = vehicle.Id };
                parking.AddTransaction(transaction);
            }

        }
        private static void Pay(string id, decimal sum)
        {
            var parking = Parking.GetInstance();
            var settings = Settings.GetInstance();
            var allVehicles = parking.GetVehicles();
            List<Vehicle> vehicles = new List<Vehicle>(allVehicles);
            var resultVehicle = from vehicle in vehicles
                                where vehicle.Id == id
                                select vehicle;
            Vehicle finalFoundVehicle = resultVehicle.FirstOrDefault();
            if (finalFoundVehicle.Balance >= sum)
            {
                finalFoundVehicle.Balance -= sum;
                parking.AddMoney(sum);
            }
            else if (finalFoundVehicle.Balance >= 0)
            {
                var difference = sum - finalFoundVehicle.Balance;
                parking.AddMoney(finalFoundVehicle.Balance);
                finalFoundVehicle.Balance -= finalFoundVehicle.Balance + difference * settings.PenaltyRate;
            }
            else
            {
                finalFoundVehicle.Balance -= sum * settings.PenaltyRate;
            }
        }
    }
}